/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <cstdint>
#include "gtest/gtest.h"
#include "native_interface.h"
#include "native_node.h"
#include "native_type.h"
#include "event_converter.h"

using namespace testing;
using namespace testing::ext;

class NativeNodeTest : public testing::Test {
public:
    static void SetUpTestCase() {};
    static void TearDownTestCase() {};
};

/**
 * @tc.name: NativeNodeTest001
 * @tc.desc: Test OH_ArkUI_NodeEvent_GetEventType function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest001, TestSize.Level1)
{
    ArkUI_NodeEvent event = { 0, -1 };
    auto eventType = OH_ArkUI_NodeEvent_GetEventType(&event);
    EXPECT_EQ(eventType, ArkUI_NodeEventType::NODE_TOUCH_EVENT);
}

/**
 * @tc.name: NativeNodeTest002
 * @tc.desc: Test OH_ArkUI_NodeEvent_GetEventType function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest002, TestSize.Level1)
{
    auto eventType = OH_ArkUI_NodeEvent_GetEventType(nullptr);
    EXPECT_EQ(eventType, ArkUI_NodeEventType::NODE_TOUCH_EVENT);
}

/**
 * @tc.name: NativeNodeTest003
 * @tc.desc: Test OH_ArkUI_NodeEvent_GetTargetId function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest003, TestSize.Level1)
{
    auto targetId = OH_ArkUI_NodeEvent_GetTargetId(nullptr);
    EXPECT_EQ(targetId, -1);
}

/**
 * @tc.name: NativeNodeTest004
 * @tc.desc: Test OH_ArkUI_NodeEvent_GetNodeHandle function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest004, TestSize.Level1)
{
    auto handle = OH_ArkUI_NodeEvent_GetNodeHandle(nullptr);
    EXPECT_EQ(handle, nullptr);
}

/**
 * @tc.name: NativeNodeTest005
 * @tc.desc: Test customNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest005, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_STACK);
    auto childNode = nodeAPI->createNode(ARKUI_NODE_STACK);
    ASSERT_NE(rootNode, nullptr);
    int32_t ret1 = nodeAPI->addChild(rootNode, childNode);
    EXPECT_EQ(ret1, ARKUI_ERROR_CODE_NO_ERROR);
    float size = 100.0f;
    float padding = 10.0f;
    float val0 = 0.0f;
    float val01 = 0.1f;
    float val05 = 0.5f;
    float val1 = 1.0f;
    float val10 = 10.0f;
    float val100 = 100.0f;
    uint32_t color = 0xFFFF0000;
    const char* pathCommands = "M100 0 L200 240 L0 240 Z";

    ArkUI_NumberValue value[] = {{.f32 = size}};
    ArkUI_AttributeItem item = {value, sizeof(value) / sizeof(ArkUI_NumberValue)};
    ArkUI_NumberValue value2[] = {{.f32 = val1}, {.f32 = val1}};
    ArkUI_AttributeItem item2 = {value2, sizeof(value2) / sizeof(ArkUI_NumberValue)};
    ArkUI_NumberValue value3[] = {{.f32 = val0}, {.f32 = val0}, {.f32 = val0}};
    ArkUI_AttributeItem item3 = {value3, sizeof(value3) / sizeof(ArkUI_NumberValue)};
    ArkUI_NumberValue value4[] = {{.f32 = padding}, {.f32 = padding}, {.f32 = padding}, {.f32 = padding}};
    ArkUI_AttributeItem item4 = {value4, sizeof(value4) / sizeof(ArkUI_NumberValue)};
    ArkUI_NumberValue value5[] = {{.f32 = val0}, {.f32 = val0}, {.f32 = val0}, {.f32 = val0}, {.f32 = val0}};
    ArkUI_AttributeItem item5 = {value5, sizeof(value5) / sizeof(ArkUI_NumberValue)};
    nodeAPI->setAttribute(rootNode, NODE_WIDTH, &item);
    auto widthVal = nodeAPI->getAttribute(rootNode, NODE_WIDTH);
    EXPECT_EQ(widthVal->value[0].f32, size);

    nodeAPI->setAttribute(rootNode, NODE_HEIGHT, &item);
    auto heightVal = nodeAPI->getAttribute(rootNode, NODE_HEIGHT);
    EXPECT_EQ(heightVal->value[0].f32, size);

    value[0].u32 = color;
    nodeAPI->setAttribute(rootNode, NODE_BACKGROUND_COLOR, &item);
    auto colorVal = nodeAPI->getAttribute(rootNode, NODE_BACKGROUND_COLOR);
    EXPECT_EQ(colorVal->value[0].u32, color);

    value[0].f32 = padding;
    nodeAPI->setAttribute(rootNode, NODE_PADDING, &item);
    auto paddingVal = nodeAPI->getAttribute(rootNode, NODE_PADDING);
    EXPECT_EQ(paddingVal->value[0].f32, padding);
    nodeAPI->setAttribute(rootNode, NODE_PADDING, &item4);
    auto paddingVal4 = nodeAPI->getAttribute(rootNode, NODE_PADDING);
    EXPECT_EQ(paddingVal4->value[0].f32, padding);

    nodeAPI->setAttribute(rootNode, NODE_MARGIN, &item);
    auto marginVal = nodeAPI->getAttribute(rootNode, NODE_MARGIN);
    EXPECT_EQ(marginVal->value[0].f32, padding);
    nodeAPI->setAttribute(rootNode, NODE_MARGIN, &item4);
    auto marginVal4 = nodeAPI->getAttribute(rootNode, NODE_MARGIN);
    EXPECT_EQ(marginVal4->value[0].f32, padding);

    item.string = "test";
    ret1 = nodeAPI->setAttribute(rootNode, NODE_ID, &item);
    EXPECT_EQ(ret1, ARKUI_ERROR_CODE_NO_ERROR);
    auto keyVal = nodeAPI->getAttribute(rootNode, NODE_ID);
    EXPECT_NE(keyVal, nullptr);

    value[0].i32 = true;
    nodeAPI->setAttribute(rootNode, NODE_ENABLED, &item);
    auto enableVal = nodeAPI->getAttribute(rootNode, NODE_ENABLED);
    EXPECT_EQ(enableVal->value[0].i32, 1);

    nodeAPI->setAttribute(rootNode, NODE_TRANSLATE, &item3);
    auto translateVal = nodeAPI->getAttribute(rootNode, NODE_TRANSLATE);
    EXPECT_EQ(translateVal->value[0].f32, val0);

    nodeAPI->setAttribute(rootNode, NODE_SCALE, &item2);
    auto scaleVal = nodeAPI->getAttribute(rootNode, NODE_SCALE);
    EXPECT_EQ(scaleVal->value[0].f32, val1);

    nodeAPI->setAttribute(rootNode, NODE_ROTATE, &item5);
    auto rotateVal = nodeAPI->getAttribute(rootNode, NODE_ROTATE);
    EXPECT_EQ(rotateVal->value[0].f32, val0);

    value[0].f32 = val0;
    nodeAPI->setAttribute(rootNode, NODE_BLUR, &item);
    auto blurVal = nodeAPI->getAttribute(rootNode, NODE_BLUR);
    EXPECT_EQ(blurVal->value[0].f32, val0);

    value[0].f32 = val0;
    nodeAPI->setAttribute(rootNode, NODE_SATURATION, &item);
    auto saturationVal = nodeAPI->getAttribute(rootNode, NODE_SATURATION);
    EXPECT_EQ(saturationVal->value[0].f32, val0);

    value[0].f32 = val0;
    nodeAPI->setAttribute(rootNode, NODE_BRIGHTNESS, &item);
    auto brightnessVal = nodeAPI->getAttribute(rootNode, NODE_BRIGHTNESS);
    EXPECT_EQ(brightnessVal->value[0].f32, val0);

    value3[0].f32 = val0;
    value3[1].i32 = ARKUI_LINEAR_GRADIENT_DIRECTION_LEFT;
    value3[2].i32 = false;
    uint32_t colors[] = {color, color};
    float stops[] = {val0, val05};
    ArkUI_ColorStop colorStop = {colors, stops, 2};
    ArkUI_ColorStop* colorStopPtr = &colorStop;
    item3.object = reinterpret_cast<void*>(colorStopPtr);
    nodeAPI->setAttribute(rootNode, NODE_LINEAR_GRADIENT, &item3);
    auto linearGradientVal = nodeAPI->getAttribute(rootNode, NODE_LINEAR_GRADIENT);
    EXPECT_EQ(linearGradientVal->value[0].f32, val0);
    
    ArkUI_NumberValue sweepGradient[] = {{.f32 = val0}, {.f32 = val0}, {.f32 = val0}, {.f32 = val0}, {.f32 = val0},
        {.i32 = true}};
    ArkUI_AttributeItem sweepGradientItem = {sweepGradient, sizeof(sweepGradient) / sizeof(ArkUI_NumberValue)};
    sweepGradientItem.object = reinterpret_cast<void*>(colorStopPtr);
    nodeAPI->setAttribute(rootNode, NODE_SWEEP_GRADIENT, &sweepGradientItem);
    auto sweepGradientVal = nodeAPI->getAttribute(rootNode, NODE_SWEEP_GRADIENT);
    EXPECT_EQ(sweepGradientVal->value[0].f32, val0);

    ArkUI_NumberValue radialGradient[] = {{.f32 = val0}, {.f32 = val0}, {.f32 = val0}, {.i32 = true}};
    ArkUI_AttributeItem radialGradientItem = {radialGradient, sizeof(radialGradient) / sizeof(ArkUI_NumberValue)};
    radialGradientItem.object = reinterpret_cast<void*>(colorStopPtr);
    nodeAPI->setAttribute(rootNode, NODE_RADIAL_GRADIENT, &radialGradientItem);
    auto radialGradientVal = nodeAPI->getAttribute(rootNode, NODE_RADIAL_GRADIENT);
    EXPECT_EQ(radialGradientVal->value[0].f32, val0);

    value[0].i32 = ARKUI_ALIGNMENT_CENTER;
    nodeAPI->setAttribute(rootNode, NODE_ALIGNMENT, &item);
    auto alignVal = nodeAPI->getAttribute(rootNode, NODE_ALIGNMENT);
    EXPECT_EQ(alignVal->value[0].i32, static_cast<int32_t>(ARKUI_ALIGNMENT_CENTER));

    value[0].f32 = val0;
    nodeAPI->setAttribute(rootNode, NODE_OPACITY, &item);
    auto opacityVal = nodeAPI->getAttribute(rootNode, NODE_OPACITY);
    EXPECT_EQ(opacityVal->value[0].f32, val0);

    value[0].f32 = val10;
    nodeAPI->setAttribute(rootNode, NODE_BORDER_WIDTH, &item);
    auto borderWidthVal = nodeAPI->getAttribute(rootNode, NODE_BORDER_WIDTH);
    EXPECT_EQ(borderWidthVal->value[0].f32, val10);
    value4[0].f32 = val10;
    value4[1].f32 = val10;
    value4[2].f32 = val10;
    value4[3].f32 = val10;
    nodeAPI->setAttribute(rootNode, NODE_BORDER_WIDTH, &item4);
    auto borderWidthVal4 = nodeAPI->getAttribute(rootNode, NODE_BORDER_WIDTH);
    EXPECT_EQ(borderWidthVal4->value[0].f32, val10);

    value[0].f32 = val01;
    nodeAPI->setAttribute(rootNode, NODE_BORDER_WIDTH_PERCENT, &item);
    borderWidthVal = nodeAPI->getAttribute(rootNode, NODE_BORDER_WIDTH_PERCENT);
    EXPECT_EQ(borderWidthVal->value[0].f32, val01);
    value4[0].f32 = val01;
    value4[1].f32 = val01;
    value4[2].f32 = val01;
    value4[3].f32 = val01;
    nodeAPI->setAttribute(rootNode, NODE_BORDER_WIDTH_PERCENT, &item4);
    borderWidthVal4 = nodeAPI->getAttribute(rootNode, NODE_BORDER_WIDTH_PERCENT);
    EXPECT_EQ(borderWidthVal4->value[0].f32, val01);

    value[0].f32 = val10;
    nodeAPI->setAttribute(rootNode, NODE_BORDER_RADIUS, &item);
    auto borderRadiusVal = nodeAPI->getAttribute(rootNode, NODE_BORDER_RADIUS);
    EXPECT_EQ(borderRadiusVal->value[0].f32, val10);
    value4[0].f32 = val10;
    value4[1].f32 = val10;
    value4[2].f32 = val10;
    value4[3].f32 = val10;
    nodeAPI->setAttribute(rootNode, NODE_BORDER_RADIUS, &item4);
    auto borderRadiusVal4 = nodeAPI->getAttribute(rootNode, NODE_BORDER_RADIUS);
    EXPECT_EQ(borderRadiusVal4->value[0].f32, val10);
    value[0].f32 = val01;
    nodeAPI->setAttribute(rootNode, NODE_BORDER_RADIUS_PERCENT, &item);
    borderRadiusVal = nodeAPI->getAttribute(rootNode, NODE_BORDER_RADIUS_PERCENT);
    EXPECT_EQ(borderRadiusVal->value[0].f32, val01);
    value4[0].f32 = val01;
    value4[1].f32 = val01;
    value4[2].f32 = val01;
    value4[3].f32 = val01;
    nodeAPI->setAttribute(rootNode, NODE_BORDER_RADIUS_PERCENT, &item4);
    borderRadiusVal4 = nodeAPI->getAttribute(rootNode, NODE_BORDER_RADIUS_PERCENT);
    EXPECT_EQ(borderRadiusVal4->value[0].f32, val01);

    value[0].u32 = color;
    nodeAPI->setAttribute(rootNode, NODE_BORDER_COLOR, &item);
    auto borderColorVal = nodeAPI->getAttribute(rootNode, NODE_BORDER_COLOR);
    EXPECT_EQ(borderColorVal->value[0].u32, color);
    value4[0].u32 = color;
    value4[1].u32 = color;
    value4[2].u32 = color;
    value4[3].u32 = color;
    nodeAPI->setAttribute(rootNode, NODE_BORDER_COLOR, &item4);
    auto borderColorVal4 = nodeAPI->getAttribute(rootNode, NODE_BORDER_COLOR);
    EXPECT_EQ(borderColorVal4->value[0].u32, color);

    value[0].i32 = ARKUI_BORDER_STYLE_SOLID;
    nodeAPI->setAttribute(rootNode, NODE_BORDER_STYLE, &item);
    auto borderStyleVal = nodeAPI->getAttribute(rootNode, NODE_BORDER_STYLE);
    EXPECT_EQ(borderStyleVal->value[0].i32, static_cast<int32_t>(ARKUI_BORDER_STYLE_SOLID));
    value4[0].i32 = ARKUI_BORDER_STYLE_SOLID;
    value4[1].i32 = ARKUI_BORDER_STYLE_SOLID;
    value4[2].i32 = ARKUI_BORDER_STYLE_SOLID;
    value4[3].i32 = ARKUI_BORDER_STYLE_SOLID;
    nodeAPI->setAttribute(rootNode, NODE_BORDER_STYLE, &item4);
    auto borderStyleVal4 = nodeAPI->getAttribute(rootNode, NODE_BORDER_STYLE);
    EXPECT_EQ(borderStyleVal4->value[0].i32, ARKUI_BORDER_STYLE_SOLID);

    value[0].f32 = val1;
    nodeAPI->setAttribute(rootNode, NODE_Z_INDEX, &item);
    auto zIndexVal = nodeAPI->getAttribute(rootNode, NODE_Z_INDEX);
    EXPECT_EQ(zIndexVal->value[0].f32, val1);

    value[0].i32 = ARKUI_VISIBILITY_VISIBLE;
    nodeAPI->setAttribute(rootNode, NODE_VISIBILITY, &item);
    auto visibilityVal = nodeAPI->getAttribute(rootNode, NODE_VISIBILITY);
    EXPECT_EQ(visibilityVal->value[0].i32, static_cast<int32_t>(ARKUI_VISIBILITY_VISIBLE));

    value[0].i32 = true;
    nodeAPI->setAttribute(rootNode, NODE_CLIP, &item);
    auto clipVal = nodeAPI->getAttribute(rootNode, NODE_CLIP);
    EXPECT_EQ(clipVal->value[0].i32, true);

    value5[0].i32 = ARKUI_CLIP_TYPE_RECTANGLE;
    value5[1].f32 = size;
    value5[2].f32 = size;
    value5[3].f32 = val10;
    value5[4].f32 = val10;
    nodeAPI->setAttribute(rootNode, NODE_CLIP_SHAPE, &item5);
    auto clipShapeVal = nodeAPI->getAttribute(rootNode, NODE_CLIP_SHAPE);
    EXPECT_EQ(clipShapeVal->value[0].i32, ARKUI_CLIP_TYPE_RECTANGLE);
    value3[0].i32 = ARKUI_CLIP_TYPE_CIRCLE;
    value3[1].f32 = size;
    value3[2].f32 = size;
    nodeAPI->setAttribute(rootNode, NODE_CLIP_SHAPE, &item3);
    clipShapeVal = nodeAPI->getAttribute(rootNode, NODE_CLIP_SHAPE);
    EXPECT_EQ(clipShapeVal->value[0].i32, ARKUI_CLIP_TYPE_CIRCLE);
    value3[0].i32 = ARKUI_CLIP_TYPE_ELLIPSE;
    value3[1].f32 = size;
    value3[2].f32 = size;
    nodeAPI->setAttribute(rootNode, NODE_CLIP_SHAPE, &item3);
    clipShapeVal = nodeAPI->getAttribute(rootNode, NODE_CLIP_SHAPE);
    EXPECT_EQ(clipShapeVal->value[0].i32, ARKUI_CLIP_TYPE_ELLIPSE);
    value3[0].i32 = ARKUI_CLIP_TYPE_PATH;
    value3[1].f32 = size;
    value3[2].f32 = size;
    item3.string = pathCommands;
    nodeAPI->setAttribute(rootNode, NODE_CLIP_SHAPE, &item3);
    clipShapeVal = nodeAPI->getAttribute(rootNode, NODE_CLIP_SHAPE);
    EXPECT_EQ(clipShapeVal->value[0].i32, ARKUI_CLIP_TYPE_PATH);

    ArkUI_NumberValue transformVal[] =
        {{.f32 = val1}, {.f32 = val0}, {.f32 = val0}, {.f32 = val0},
        {.f32 = val0}, {.f32 = val1}, {.f32 = val0}, {.f32 = val0},
        {.f32 = val0}, {.f32 = val0}, {.f32 = val1}, {.f32 = val0},
        {.f32 = val0}, {.f32 = val0}, {.f32 = val0}, {.f32 = val1}};
    ArkUI_AttributeItem transformItem = {transformVal, sizeof(transformVal) / sizeof(ArkUI_NumberValue)};
    nodeAPI->setAttribute(rootNode, NODE_TRANSFORM, &transformItem);
    auto transformResult = nodeAPI->getAttribute(rootNode, NODE_TRANSFORM);
    EXPECT_EQ(transformResult->value[0].f32, val1);

    value[0].i32 = ARKUI_HIT_TEST_MODE_DEFAULT;
    nodeAPI->setAttribute(rootNode, NODE_HIT_TEST_BEHAVIOR, &item);
    auto hitVal = nodeAPI->getAttribute(rootNode, NODE_HIT_TEST_BEHAVIOR);
    EXPECT_EQ(hitVal->value[0].i32, static_cast<int32_t>(ARKUI_HIT_TEST_MODE_DEFAULT));

    value2[0].f32 = val10;
    value2[1].f32 = val10;
    nodeAPI->setAttribute(rootNode, NODE_POSITION, &item2);
    auto positionVal = nodeAPI->getAttribute(rootNode, NODE_POSITION);
    EXPECT_EQ(positionVal->value[0].f32, val10);

    value[0].i32 = ARKUI_SHADOW_STYLE_OUTER_DEFAULT_MD;
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_SHADOW, &item), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_NE(nodeAPI->getAttribute(rootNode, NODE_SHADOW), nullptr);
    

    ArkUI_NumberValue customShadowVal[] = {{.f32 = val10}, {.i32 = true}, {.f32 = val0}, {.f32 = val0},
        {.i32 = ARKUI_SHADOW_TYPE_COLOR}, {.u32 = color}, {.u32 = 1}};
    ArkUI_AttributeItem customShadowItem = {customShadowVal, sizeof(customShadowVal) / sizeof(ArkUI_NumberValue)};
    nodeAPI->setAttribute(rootNode, NODE_CUSTOM_SHADOW, &customShadowItem);
    auto customshadowResult = nodeAPI->getAttribute(rootNode, NODE_CUSTOM_SHADOW);
    EXPECT_EQ(customshadowResult->value[0].f32, val10);

    value2[0].f32 = size;
    value2[1].f32 = size;
    nodeAPI->setAttribute(rootNode, NODE_BACKGROUND_IMAGE_SIZE, &item2);
    auto imagesizeVal = nodeAPI->getAttribute(rootNode, NODE_BACKGROUND_IMAGE_SIZE);
    EXPECT_EQ(imagesizeVal->value[0].f32, size);

    value[0].i32 = ARKUI_IMAGE_SIZE_COVER;
    nodeAPI->setAttribute(rootNode, NODE_BACKGROUND_IMAGE_SIZE_WITH_STYLE, &item);
    auto imagesizeStyleVal = nodeAPI->getAttribute(rootNode, NODE_BACKGROUND_IMAGE_SIZE_WITH_STYLE);
    EXPECT_EQ(imagesizeStyleVal->value[0].i32, static_cast<int32_t>(ARKUI_IMAGE_SIZE_COVER));

    ArkUI_NumberValue backgroundBlurVal[] = {{.i32 = ARKUI_BLUR_STYLE_THIN}, {.i32 = ARKUI_COLOR_MODE_SYSTEM},
        {.i32 = ARKUI_ADAPTIVE_COLOR_DEFAULT}, {.f32 = val05}, {.f32 = val0}, {.f32 = val0}};
    ArkUI_AttributeItem backgroundBlurItem = {backgroundBlurVal, sizeof(backgroundBlurVal) / sizeof(ArkUI_NumberValue)};
    nodeAPI->setAttribute(rootNode, NODE_BACKGROUND_BLUR_STYLE, &backgroundBlurItem);
    auto backgroundBlurResult = nodeAPI->getAttribute(rootNode, NODE_BACKGROUND_BLUR_STYLE);
    EXPECT_EQ(backgroundBlurResult->value[0].i32, static_cast<int32_t>(ARKUI_BLUR_STYLE_THIN));
    nodeAPI->setAttribute(rootNode, NODE_FOREGROUND_BLUR_STYLE, &backgroundBlurItem);
    auto foregroundBlurStyleVal = nodeAPI->getAttribute(rootNode, NODE_FOREGROUND_BLUR_STYLE);
    EXPECT_EQ(foregroundBlurStyleVal->value[0].i32, static_cast<int32_t>(ARKUI_BLUR_STYLE_THIN));

    ArkUI_NumberValue transformCenter[] = {{.f32 = val0}, {.i32 = val0}, {.f32 = val0}, {.f32 = val05},
        {.f32 = val05}, {.f32 = val0}};
    ArkUI_AttributeItem transformCenterItem = {transformCenter, sizeof(transformCenter) / sizeof(ArkUI_NumberValue)};
    nodeAPI->setAttribute(rootNode, NODE_TRANSFORM_CENTER, &transformCenterItem);
    auto transformCenterVal = nodeAPI->getAttribute(rootNode, NODE_TRANSFORM_CENTER);
    EXPECT_EQ(transformCenterVal->value[0].f32, val0);

    ArkUI_NumberValue opacityTransition[] = {{.f32 = val0}, {.i32 = 1000}, {.i32 = ARKUI_CURVE_LINEAR}, {.i32 = 1000},
        {.i32 = 1}, {.i32 = ARKUI_ANIMATION_PLAY_MODE_NORMAL}, {.f32 = val1}};
    ArkUI_AttributeItem opacityTransitionItem =
        {opacityTransition, sizeof(opacityTransition) / sizeof(ArkUI_NumberValue)};
    nodeAPI->setAttribute(rootNode, NODE_OPACITY_TRANSITION, &opacityTransitionItem);
    auto opacityTransitionVal = nodeAPI->getAttribute(rootNode, NODE_OPACITY_TRANSITION);
    EXPECT_EQ(opacityTransitionVal->value[0].f32, val0);

    ArkUI_NumberValue rotateTransition[] = {{.f32 = val0}, {.f32 = val0}, {.f32 = val0}, {.f32 = val10},
        {.f32 = val0}, {.i32 = 1000}, {.i32 = ARKUI_CURVE_LINEAR}, {.i32 = 1000},
        {.i32 = 1}, {.i32 = ARKUI_ANIMATION_PLAY_MODE_NORMAL}, {.f32 = val1}};
    ArkUI_AttributeItem rotateTransitionItem =
        {rotateTransition, sizeof(rotateTransition) / sizeof(ArkUI_NumberValue)};
    nodeAPI->setAttribute(rootNode, NODE_ROTATE_TRANSITION, &rotateTransitionItem);
    auto rotateTransitionVal = nodeAPI->getAttribute(rootNode, NODE_ROTATE_TRANSITION);
    EXPECT_EQ(rotateTransitionVal->value[0].f32, val0);

    ArkUI_NumberValue scaleTransition[] = {{.f32 = val0}, {.f32 = val0}, {.f32 = val0}, {.f32 = val10},
        {.f32 = val0}, {.i32 = 1000}, {.i32 = ARKUI_CURVE_LINEAR}, {.i32 = 1000},
        {.i32 = 1}, {.i32 = ARKUI_ANIMATION_PLAY_MODE_NORMAL}, {.f32 = val1}};
    ArkUI_AttributeItem scaleTransitionItem =
        {scaleTransition, sizeof(scaleTransition) / sizeof(ArkUI_NumberValue)};
    nodeAPI->setAttribute(rootNode, NODE_SCALE_TRANSITION, &scaleTransitionItem);
    auto scaleTransitionVal = nodeAPI->getAttribute(rootNode, NODE_SCALE_TRANSITION);
    EXPECT_EQ(scaleTransitionVal->value[0].f32, val0);

    ArkUI_NumberValue translateTransition[] = {{.f32 = val0}, {.f32 = val0}, {.f32 = val0},
        {.i32 = 1000}, {.i32 = ARKUI_CURVE_LINEAR}, {.i32 = 1000},
        {.i32 = 1}, {.i32 = ARKUI_ANIMATION_PLAY_MODE_NORMAL}, {.f32 = val1}};
    ArkUI_AttributeItem translateTransitionItem =
        {translateTransition, sizeof(translateTransition) / sizeof(ArkUI_NumberValue)};
    nodeAPI->setAttribute(rootNode, NODE_TRANSLATE_TRANSITION, &translateTransitionItem);
    auto translateTransitionVal = nodeAPI->getAttribute(rootNode, NODE_TRANSLATE_TRANSITION);
    EXPECT_EQ(translateTransitionVal->value[0].f32, val0);

    ArkUI_NumberValue moveTransition[] = {{.i32 = ARKUI_TRANSITION_EDGE_TOP},
        {.i32 = 1000}, {.i32 = ARKUI_CURVE_LINEAR}, {.i32 = 1000},
        {.i32 = 1}, {.i32 = ARKUI_ANIMATION_PLAY_MODE_NORMAL}, {.f32 = val1}};
    ArkUI_AttributeItem moveTransitionItem =
        {moveTransition, sizeof(moveTransition) / sizeof(ArkUI_NumberValue)};
    nodeAPI->setAttribute(rootNode, NODE_MOVE_TRANSITION, &moveTransitionItem);
    auto moveTransitionVal = nodeAPI->getAttribute(rootNode, NODE_MOVE_TRANSITION);
    EXPECT_EQ(moveTransitionVal->value[0].i32, static_cast<int32_t>(ARKUI_TRANSITION_EDGE_TOP));

    value[0].i32 = true;
    nodeAPI->setAttribute(rootNode, NODE_FOCUSABLE, &item);
    auto focusableVal = nodeAPI->getAttribute(rootNode, NODE_FOCUSABLE);
    EXPECT_EQ(focusableVal->value[0].i32, true);

    value[0].i32 = true;
    nodeAPI->setAttribute(rootNode, NODE_DEFAULT_FOCUS, &item);
    auto defaultFocusVal = nodeAPI->getAttribute(rootNode, NODE_DEFAULT_FOCUS);
    EXPECT_EQ(defaultFocusVal->value[0].i32, true);

    value4[0].f32 = val0;
    value4[1].f32 = val0;
    value4[2].f32 = val100;
    value4[3].f32 = val100;
    nodeAPI->setAttribute(rootNode, NODE_RESPONSE_REGION, &item4);
    auto responseRegionVal = nodeAPI->getAttribute(rootNode, NODE_RESPONSE_REGION);
    EXPECT_EQ(responseRegionVal->value[0].f32, val0);

    value3[0].i32 = ARKUI_ALIGNMENT_TOP_START;
    value3[1].f32 = val0;
    value3[2].f32 = val0;
    item3.string = "test";
    nodeAPI->setAttribute(rootNode, NODE_OVERLAY, &item3);
    auto overlayVal = nodeAPI->getAttribute(rootNode, NODE_OVERLAY);
    EXPECT_EQ(overlayVal->value[0].i32, static_cast<int32_t>(ARKUI_ALIGNMENT_TOP_START));

    ArkUI_NumberValue mask[] = {{.u32 = color}, {.u32 = color}, {.f32 = val10}, {.i32 = ARKUI_MASK_TYPE_RECTANGLE},
        {.f32 = size}, {.f32 = size}, {.f32 = val10}, {.f32 = val10}};
    ArkUI_AttributeItem maskItem = {mask, sizeof(mask) / sizeof(ArkUI_NumberValue)};
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_MASK, &maskItem), ARKUI_ERROR_CODE_NO_ERROR);
    auto maskVal = nodeAPI->getAttribute(rootNode, NODE_MASK);
    EXPECT_EQ(maskVal->value[0].u32, color);
    mask[3].i32 = ARKUI_MASK_TYPE_CIRCLE;
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_MASK, &maskItem), ARKUI_ERROR_CODE_NO_ERROR);
    maskVal = nodeAPI->getAttribute(rootNode, NODE_MASK);
    EXPECT_EQ(maskVal->value[0].u32, color);
    mask[3].i32 = ARKUI_MASK_TYPE_ELLIPSE;
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_MASK, &maskItem), ARKUI_ERROR_CODE_NO_ERROR);
    maskVal = nodeAPI->getAttribute(rootNode, NODE_MASK);
    EXPECT_EQ(maskVal->value[0].u32, color);
    mask[3].i32 = ARKUI_MASK_TYPE_PATH;
    maskItem.string = pathCommands;
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_MASK, &maskItem), ARKUI_ERROR_CODE_NO_ERROR);
    maskVal = nodeAPI->getAttribute(rootNode, NODE_MASK);
    EXPECT_EQ(maskVal->value[0].u32, color);

    value4[0].i32 = ARKUI_MASK_TYPE_PROGRESS;
    value4[1].f32 = val10;
    value4[2].f32 = val100;
    value4[3].u32 = color;
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_MASK, &item4), ARKUI_ERROR_CODE_NO_ERROR);
    maskVal = nodeAPI->getAttribute(rootNode, NODE_MASK);
    EXPECT_EQ(maskVal->value[0].i32, static_cast<int32_t>(ARKUI_MASK_TYPE_PROGRESS));

    value2[0].i32 = ARKUI_BLEND_MODE_NONE;
    value2[1].i32 = BLEND_APPLY_TYPE_FAST;
    nodeAPI->setAttribute(rootNode, NODE_BLEND_MODE, &item2);
    auto blendModeVal = nodeAPI->getAttribute(rootNode, NODE_BLEND_MODE);
    EXPECT_EQ(blendModeVal->value[0].i32, static_cast<int32_t>(ARKUI_BLEND_MODE_NONE));

    value[0].i32 = ARKUI_DIRECTION_LTR;
    nodeAPI->setAttribute(rootNode, NODE_DIRECTION, &item);
    auto directionVal = nodeAPI->getAttribute(rootNode, NODE_DIRECTION);
    EXPECT_EQ(directionVal->value[0].i32, static_cast<int32_t>(ARKUI_DIRECTION_LTR));

    value4[0].f32 = val0;
    value4[1].f32 = size;
    value4[2].f32 = val0;
    value4[3].f32 = size;
    nodeAPI->setAttribute(rootNode, NODE_CONSTRAINT_SIZE, &item4);
    auto constraintSizeVal = nodeAPI->getAttribute(rootNode, NODE_CONSTRAINT_SIZE);
    EXPECT_EQ(constraintSizeVal->value[0].f32, val0);

    value[0].f32 = val05;
    nodeAPI->setAttribute(rootNode, NODE_GRAY_SCALE, &item);
    auto grayScaleVal = nodeAPI->getAttribute(rootNode, NODE_GRAY_SCALE);
    EXPECT_EQ(grayScaleVal->value[0].f32, val05);

    value[0].f32 = val05;
    nodeAPI->setAttribute(rootNode, NODE_INVERT, &item);
    auto invertVal = nodeAPI->getAttribute(rootNode, NODE_INVERT);
    EXPECT_EQ(invertVal->value[0].f32, val05);

    value[0].f32 = val05;
    nodeAPI->setAttribute(rootNode, NODE_SEPIA, &item);
    auto sepiaVal = nodeAPI->getAttribute(rootNode, NODE_SEPIA);
    EXPECT_EQ(sepiaVal->value[0].f32, val05);

    value[0].f32 = val05;
    nodeAPI->setAttribute(rootNode, NODE_CONTRAST, &item);
    auto contrastVal = nodeAPI->getAttribute(rootNode, NODE_CONTRAST);
    EXPECT_EQ(contrastVal->value[0].f32, val05);

    value2[0].u32 = color;
    value2[1].i32 = ARKUI_COLOR_STRATEGY_INVERT;
    nodeAPI->setAttribute(rootNode, NODE_FOREGROUND_COLOR, &item2);
    auto foregroundColorVal = nodeAPI->getAttribute(rootNode, NODE_FOREGROUND_COLOR);
    EXPECT_EQ(foregroundColorVal->value[0].u32, color);

    value2[0].f32 = val10;
    value2[1].f32 = val10;
    nodeAPI->setAttribute(rootNode, NODE_OFFSET, &item2);
    auto offsetVal = nodeAPI->getAttribute(rootNode, NODE_OFFSET);
    EXPECT_EQ(offsetVal->value[0].f32, val10);

    value2[0].f32 = val10;
    value2[1].f32 = val10;
    nodeAPI->setAttribute(rootNode, NODE_MARK_ANCHOR, &item2);
    auto maskAnchorVal = nodeAPI->getAttribute(rootNode, NODE_MARK_ANCHOR);
    EXPECT_EQ(maskAnchorVal->value[0].f32, val10);

    value2[0].f32 = val10;
    value2[1].f32 = val10;
    nodeAPI->setAttribute(rootNode, NODE_BACKGROUND_IMAGE_POSITION, &item2);
    auto backgroundImagePositionVal = nodeAPI->getAttribute(rootNode, NODE_BACKGROUND_IMAGE_POSITION);
    EXPECT_EQ(backgroundImagePositionVal->value[0].f32, val10);

    value[0].i32 = 0;
    nodeAPI->setAttribute(rootNode, NODE_ACCESSIBILITY_GROUP, &item);
    auto accessibilityGroupVal = nodeAPI->getAttribute(rootNode, NODE_ACCESSIBILITY_GROUP);
    EXPECT_EQ(accessibilityGroupVal->value[0].i32, 0);

    item.string = "test";
    nodeAPI->setAttribute(rootNode, NODE_ACCESSIBILITY_TEXT, &item);
    auto accessibilityTextVal = nodeAPI->getAttribute(rootNode, NODE_ACCESSIBILITY_TEXT);
    EXPECT_NE(accessibilityTextVal, nullptr);
    nodeAPI->setAttribute(rootNode, NODE_ACCESSIBILITY_DESCRIPTION, &item);
    auto accessibilityDescVal = nodeAPI->getAttribute(rootNode, NODE_ACCESSIBILITY_DESCRIPTION);
    EXPECT_NE(accessibilityDescVal, nullptr);

    value[0].i32 = ARKUI_ACCESSIBILITY_MODE_AUTO;
    nodeAPI->setAttribute(rootNode, NODE_ACCESSIBILITY_MODE, &item);
    auto accessibilityModeVal = nodeAPI->getAttribute(rootNode, NODE_ACCESSIBILITY_MODE);
    EXPECT_EQ(accessibilityModeVal->value[0].i32, static_cast<int32_t>(ARKUI_ACCESSIBILITY_MODE_AUTO));

    value[0].i32 = false;
    nodeAPI->setAttribute(rootNode, NODE_FOCUS_STATUS, &item);
    auto focsuStatusVal = nodeAPI->getAttribute(rootNode, NODE_FOCUS_STATUS);
    EXPECT_EQ(focsuStatusVal->value[0].i32, 0);

    value[0].f32 = val05;
    nodeAPI->setAttribute(rootNode, NODE_ASPECT_RATIO, &item);
    auto aspectRatioVal = nodeAPI->getAttribute(rootNode, NODE_ASPECT_RATIO);
    EXPECT_EQ(aspectRatioVal->value[0].f32, val05);

    value[0].f32 = val05;
    nodeAPI->setAttribute(rootNode, NODE_LAYOUT_WEIGHT, &item);
    auto layoutWeightVal = nodeAPI->getAttribute(rootNode, NODE_LAYOUT_WEIGHT);
    EXPECT_EQ(layoutWeightVal->value[0].f32, val05);

    value[0].u32 = 0;
    nodeAPI->setAttribute(rootNode, NODE_DISPLAY_PRIORITY, &item);
    auto displayPriorityVal = nodeAPI->getAttribute(rootNode, NODE_DISPLAY_PRIORITY);
    EXPECT_EQ(displayPriorityVal->value[0].u32, 0);

    value4[0].f32 = val10;
    value4[1].f32 = val10;
    value4[2].f32 = val10;
    value4[3].f32 = val10;
    nodeAPI->setAttribute(rootNode, NODE_OUTLINE_WIDTH, &item4);
    auto outlineWidthVal = nodeAPI->getAttribute(rootNode, NODE_OUTLINE_WIDTH);
    EXPECT_EQ(outlineWidthVal->value[0].f32, val10);

    value[0].f32 = val05;
    nodeAPI->setAttribute(rootNode, NODE_WIDTH_PERCENT, &item);
    auto widthPercentVal = nodeAPI->getAttribute(rootNode, NODE_WIDTH_PERCENT);
    EXPECT_EQ(widthPercentVal->value[0].f32, val05);

    value[0].f32 = val05;
    nodeAPI->setAttribute(rootNode, NODE_HEIGHT_PERCENT, &item);
    auto heightPercentVal = nodeAPI->getAttribute(rootNode, NODE_HEIGHT_PERCENT);
    EXPECT_EQ(heightPercentVal->value[0].f32, val05);

    value[0].f32 = val01;
    nodeAPI->setAttribute(rootNode, NODE_PADDING_PERCENT, &item);
    auto paddingPercentVal = nodeAPI->getAttribute(rootNode, NODE_PADDING_PERCENT);
    EXPECT_EQ(paddingPercentVal->value[0].f32, val01);
    value4[0].f32 = val01;
    value4[1].f32 = val01;
    value4[2].f32 = val01;
    value4[3].f32 = val01;
    nodeAPI->setAttribute(rootNode, NODE_PADDING_PERCENT, &item4);
    auto paddingPercentVal4 = nodeAPI->getAttribute(rootNode, NODE_PADDING_PERCENT);
    EXPECT_EQ(paddingPercentVal4->value[0].f32, val01);

    nodeAPI->setAttribute(rootNode, NODE_MARGIN_PERCENT, &item);
    auto marginPercentVal = nodeAPI->getAttribute(rootNode, NODE_MARGIN_PERCENT);
    EXPECT_EQ(marginPercentVal->value[0].f32, val01);
    nodeAPI->setAttribute(rootNode, NODE_MARGIN_PERCENT, &item4);
    auto marginPercentVal4 = nodeAPI->getAttribute(rootNode, NODE_MARGIN_PERCENT);
    EXPECT_EQ(marginPercentVal4->value[0].f32, val01);
    
    value[0].i32 = false;
    item.string = "test";
    nodeAPI->setAttribute(rootNode, NODE_GEOMETRY_TRANSITION, &item);
    auto geometryTransitionVal = nodeAPI->getAttribute(rootNode, NODE_GEOMETRY_TRANSITION);
    EXPECT_EQ(geometryTransitionVal->value[0].i32, 0);

    value[0].i32 = ARKUI_RENDER_FIT_CENTER;
    nodeAPI->setAttribute(rootNode, NODE_RENDER_FIT, &item);
    auto renderFitVal = nodeAPI->getAttribute(rootNode, NODE_RENDER_FIT);
    EXPECT_EQ(renderFitVal->value[0].i32, static_cast<int32_t>(ARKUI_RENDER_FIT_CENTER));

    value[0].u32 = color;
    nodeAPI->setAttribute(rootNode, NODE_OUTLINE_COLOR, &item);
    auto outlineColorVal = nodeAPI->getAttribute(rootNode, NODE_OUTLINE_COLOR);
    EXPECT_EQ(outlineColorVal->value[0].u32, color);
    value4[0].u32 = color;
    value4[1].u32 = color;
    value4[2].u32 = color;
    value4[3].u32 = color;
    nodeAPI->setAttribute(rootNode, NODE_OUTLINE_COLOR, &item4);
    auto outlineColorVal4 = nodeAPI->getAttribute(rootNode, NODE_OUTLINE_COLOR);
    EXPECT_EQ(outlineColorVal4->value[0].u32, color);

    value2[0].f32 = size;
    value2[1].f32 = size;
    nodeAPI->setAttribute(rootNode, NODE_SIZE, &item2);
    auto sizeVal = nodeAPI->getAttribute(rootNode, NODE_SIZE);
    EXPECT_EQ(sizeVal->value[0].f32, size);

    value[0].i32 = false;
    nodeAPI->setAttribute(rootNode, NODE_RENDER_GROUP, &item);
    auto renderGroupVal = nodeAPI->getAttribute(rootNode, NODE_RENDER_GROUP);
    EXPECT_EQ(renderGroupVal->value[0].i32, 0);

    value[0].u32 = color;
    nodeAPI->setAttribute(rootNode, NODE_COLOR_BLEND, &item);
    auto colorBlendVal = nodeAPI->getAttribute(rootNode, NODE_COLOR_BLEND);
    EXPECT_EQ(colorBlendVal->value[0].u32, color);
    
    value4[0].i32 = 0;
    value4[1].i32 = 0;
    value4[2].i32 = 100;
    value4[3].i32 = 100;
    nodeAPI->setAttribute(rootNode, NODE_LAYOUT_RECT, &item4);
    auto layoutRectVal = nodeAPI->getAttribute(rootNode, NODE_LAYOUT_RECT);
    EXPECT_EQ(layoutRectVal->value[0].i32, 0);

    value[0].i32 = 1;
    nodeAPI->setAttribute(rootNode, NODE_FOCUS_ON_TOUCH, &item);
    auto onTouchVal = nodeAPI->getAttribute(rootNode, NODE_FOCUS_ON_TOUCH);
    EXPECT_EQ(onTouchVal->value[0].i32, 1);

    value[0].f32 = val01;
    nodeAPI->setAttribute(rootNode, NODE_BORDER_WIDTH_PERCENT, &item);
    auto borderWidthPercentVal = nodeAPI->getAttribute(rootNode, NODE_BORDER_WIDTH_PERCENT);
    EXPECT_EQ(borderWidthPercentVal->value[0].f32, val01);
    value4[0].f32 = val01;
    value4[1].f32 = val01;
    value4[2].f32 = val01;
    value4[3].f32 = val01;
    nodeAPI->setAttribute(rootNode, NODE_BORDER_WIDTH_PERCENT, &item4);
    auto borderWidthPercentVal4 = nodeAPI->getAttribute(rootNode, NODE_BORDER_WIDTH_PERCENT);
    EXPECT_EQ(borderWidthPercentVal4->value[0].f32, val01);

    value[0].f32 = val01;
    nodeAPI->setAttribute(rootNode, NODE_BORDER_RADIUS_PERCENT, &item);
    auto borderRadiusPercentVal = nodeAPI->getAttribute(rootNode, NODE_BORDER_RADIUS_PERCENT);
    EXPECT_EQ(borderRadiusPercentVal->value[0].f32, val01);
    value4[0].f32 = val01;
    value4[1].f32 = val01;
    value4[2].f32 = val01;
    value4[3].f32 = val01;
    nodeAPI->setAttribute(rootNode, NODE_BORDER_RADIUS_PERCENT, &item4);
    auto borderRadiusPercentVal4 = nodeAPI->getAttribute(rootNode, NODE_BORDER_RADIUS_PERCENT);
    EXPECT_EQ(borderRadiusPercentVal4->value[0].f32, val01);

    value[0].i32 = 1;
    nodeAPI->setAttribute(rootNode, NODE_ACCESSIBILITY_ID, &item);
    auto accessibilityIdVal = nodeAPI->getAttribute(rootNode, NODE_ACCESSIBILITY_ID);
    EXPECT_EQ(accessibilityIdVal->value[0].i32, 1);

    value[0].u32 = ARKUI_ACCESSIBILITY_ACTION_CLICK;
    nodeAPI->setAttribute(rootNode, NODE_ACCESSIBILITY_ACTIONS, &item);
    auto accessibilityActionVal = nodeAPI->getAttribute(rootNode, NODE_ACCESSIBILITY_ACTIONS);
    EXPECT_EQ(accessibilityActionVal->value[0].u32, ARKUI_ACCESSIBILITY_ACTION_CLICK);

    value[0].u32 = ARKUI_NODE_STACK;
    nodeAPI->setAttribute(rootNode, NODE_ACCESSIBILITY_ROLE, &item);
    auto accessibilityRoleVal = nodeAPI->getAttribute(rootNode, NODE_ACCESSIBILITY_ROLE);
    EXPECT_EQ(accessibilityRoleVal->value[0].u32, ARKUI_NODE_STACK);

    value2[0].u32 = ARKUI_SAFE_AREA_TYPE_SYSTEM;
    value2[1].u32 = ARKUI_SAFE_AREA_EDGE_TOP;
    nodeAPI->setAttribute(rootNode, NODE_EXPAND_SAFE_AREA, &item2);
    auto extendSafeAreaVal = nodeAPI->getAttribute(rootNode, NODE_EXPAND_SAFE_AREA);
    EXPECT_EQ(extendSafeAreaVal->value[0].u32, ARKUI_SAFE_AREA_TYPE_SYSTEM);

    value[0].i32 = ARKUI_ALIGNMENT_CENTER;
    nodeAPI->setAttribute(rootNode, NODE_STACK_ALIGN_CONTENT, &item);
    auto stackAlignVal = nodeAPI->getAttribute(rootNode, NODE_STACK_ALIGN_CONTENT);
    EXPECT_EQ(stackAlignVal->value[0].i32, static_cast<int32_t>(ARKUI_ALIGNMENT_CENTER));

    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_WIDTH), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_HEIGHT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BACKGROUND_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_PADDING), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_MARGIN), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_ID), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_ENABLED), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TRANSLATE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SCALE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_ROTATE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BRIGHTNESS), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SATURATION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BLUR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_LINEAR_GRADIENT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_ALIGNMENT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_OPACITY), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BORDER_WIDTH), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BORDER_RADIUS), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BORDER_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BORDER_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_Z_INDEX), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_VISIBILITY), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_CLIP), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_CLIP_SHAPE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TRANSFORM), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_HIT_TEST_BEHAVIOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_POSITION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SHADOW), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_CUSTOM_SHADOW), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BACKGROUND_IMAGE_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BACKGROUND_IMAGE_SIZE_WITH_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BACKGROUND_BLUR_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TRANSFORM_CENTER), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_OPACITY_TRANSITION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_ROTATE_TRANSITION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SCALE_TRANSITION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TRANSLATE_TRANSITION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_MOVE_TRANSITION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FOCUSABLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_DEFAULT_FOCUS), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_RESPONSE_REGION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_OVERLAY), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWEEP_GRADIENT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_RADIAL_GRADIENT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_MASK), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BLEND_MODE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_DIRECTION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_CONSTRAINT_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_GRAY_SCALE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_INVERT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SEPIA), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_CONTRAST), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FOREGROUND_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_OFFSET), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_MARK_ANCHOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BACKGROUND_IMAGE_POSITION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_ACCESSIBILITY_GROUP), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_ACCESSIBILITY_TEXT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_ACCESSIBILITY_MODE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_ACCESSIBILITY_DESCRIPTION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FOCUS_STATUS), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_ASPECT_RATIO), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_LAYOUT_WEIGHT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_DISPLAY_PRIORITY), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_OUTLINE_WIDTH), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_WIDTH_PERCENT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_HEIGHT_PERCENT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_PADDING_PERCENT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_MARGIN_PERCENT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_GEOMETRY_TRANSITION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_RENDER_FIT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_OUTLINE_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_RENDER_GROUP), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_COLOR_BLEND), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FOREGROUND_BLUR_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_LAYOUT_RECT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FOCUS_ON_TOUCH), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BORDER_WIDTH_PERCENT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BORDER_RADIUS_PERCENT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_ACCESSIBILITY_ID), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_ACCESSIBILITY_ACTIONS), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_ACCESSIBILITY_ROLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_ACCESSIBILITY_STATE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_ACCESSIBILITY_VALUE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_EXPAND_SAFE_AREA), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_VISIBLE_AREA_CHANGE_RATIO), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TRANSITION), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest006
 * @tc.desc: Test ConvertOriginEventType function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest006, TestSize.Level1)
{
    int32_t ret;
    int32_t nodeType = static_cast<int32_t>(ARKUI_NODE_TEXT_INPUT);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_INPUT_ON_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_INPUT_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_INPUT_ON_SUBMIT, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_INPUT_SUBMIT));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_INPUT_ON_CUT, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_INPUT_CUT));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_INPUT_ON_PASTE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_INPUT_PASTE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_INPUT_ON_TEXT_SELECTION_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_INPUT_TEXT_SELECTION_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_INPUT_ON_EDIT_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_INPUT_EDIT_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_INPUT_ON_CONTENT_SIZE_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_INPUT_CONTENT_SIZE_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_INPUT_ON_INPUT_FILTER_ERROR, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_INPUT_INPUT_FILTER_ERROR));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_INPUT_ON_CONTENT_SCROLL, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_INPUT_CONTENT_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_INPUT_ON_WILL_INSERT, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_INPUT_WILL_INSERT));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_INPUT_ON_DID_INSERT, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_INPUT_DID_INSERT));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_INPUT_ON_WILL_DELETE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_INPUT_WILL_DELETE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_INPUT_ON_DID_DELETE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_INPUT_DID_DELETE));

    nodeType = static_cast<int32_t>(ARKUI_NODE_SCROLL);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_SCROLL, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_SCROLL_FRAME_BEGIN, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_SCROLL_FRAME_BEGIN));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_WILL_SCROLL, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_SCROLL_WILL_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_DID_SCROLL, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_SCROLL_DID_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_SCROLL_START, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_SCROLL_START));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_SCROLL_STOP, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_SCROLL_STOP));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_SCROLL_EDGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_SCROLL_EDGE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_REACH_START, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_SCROLL_REACH_START));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_REACH_END, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_SCROLL_REACH_END));

    nodeType = static_cast<int32_t>(ARKUI_NODE_LIST);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_SCROLL, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_LIST_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_SCROLL_FRAME_BEGIN, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_LIST_SCROLL_FRAME_BEGIN));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_SCROLL_START, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_LIST_SCROLL_START));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_SCROLL_STOP, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_LIST_SCROLL_STOP));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_REACH_START, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_LIST_REACH_START));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_REACH_END, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_LIST_REACH_END));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_LIST_ON_SCROLL_INDEX, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_LIST_SCROLL_INDEX));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_LIST_ON_DID_SCROLL, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_LIST_DID_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_LIST_ON_WILL_SCROLL, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_LIST_WILL_SCROLL));
    
    nodeType = static_cast<int32_t>(ARKUI_NODE_WATER_FLOW);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_SCROLL_FRAME_BEGIN, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_WATER_FLOW_SCROLL_FRAME_BEGIN));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_SCROLL_START, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_WATER_FLOW_SCROLL_START));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_SCROLL_STOP, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_WATER_FLOW_SCROLL_STOP));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_ON_WILL_SCROLL, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_WATER_FLOW_WILL_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_REACH_START, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_WATER_FLOW_REACH_START));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SCROLL_EVENT_ON_REACH_END, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_WATER_FLOW_REACH_END));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_WATER_FLOW_ON_DID_SCROLL, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_WATER_FLOW_DID_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_WATER_FLOW_ON_SCROLL_INDEX, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_WATER_FLOW_SCROLL_INDEX));

    nodeType = static_cast<int32_t>(ARKUI_NODE_STACK);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_EVENT_ON_APPEAR, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_APPEAR));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_EVENT_ON_DISAPPEAR, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_DISAPPEAR));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_EVENT_ON_AREA_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_AREA_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_ON_FOCUS, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_FOCUS));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_ON_BLUR, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_BLUR));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TOUCH_EVENT, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TOUCH));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_ON_CLICK, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_CLICK));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_ON_TOUCH_INTERCEPT, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TOUCH_INTERCEPT));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_EVENT_ON_VISIBLE_AREA_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_VISIBLE_AREA_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_ON_HOVER, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_HOVER));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_ON_MOUSE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_MOUSE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_EVENT_ON_ATTACH, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_ATTACH));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_EVENT_ON_DETACH, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_DETACH));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_ON_ACCESSIBILITY_ACTIONS, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_ACCESSIBILITY_ACTIONS));

    nodeType = static_cast<int32_t>(ARKUI_NODE_TEXT_AREA);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_AREA_ON_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXTAREA_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_AREA_ON_INPUT_FILTER_ERROR, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_AREA_INPUT_FILTER_ERROR));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_AREA_ON_EDIT_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXTAREA_EDIT_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_AREA_ON_SUBMIT, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXTAREA_ON_SUBMIT));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_AREA_ON_CONTENT_SIZE_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXTAREA_CONTENT_SIZE_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_AREA_ON_PASTE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXTAREA_PASTE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_AREA_ON_TEXT_SELECTION_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXTAREA_TEXT_SELECTION_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_AREA_ON_CONTENT_SCROLL, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_AREA_CONTENT_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_AREA_ON_WILL_INSERT, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_AREA_WILL_INSERT));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_AREA_ON_DID_INSERT, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_AREA_DID_INSERT));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_AREA_ON_WILL_DELETE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_AREA_WILL_DELETE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_AREA_ON_DID_DELETE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_AREA_DID_DELETE));


    nodeType = static_cast<int32_t>(ARKUI_NODE_REFRESH);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_REFRESH_STATE_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_REFRESH_STATE_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_REFRESH_ON_REFRESH, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_REFRESH_REFRESHING));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_REFRESH_ON_OFFSET_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_REFRESH_ON_OFFSET_CHANGE));

    nodeType = static_cast<int32_t>(ARKUI_NODE_TOGGLE);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TOGGLE_ON_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TOGGLE_CHANGE));

    nodeType = static_cast<int32_t>(ARKUI_NODE_IMAGE);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_IMAGE_ON_COMPLETE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_IMAGE_COMPLETE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_IMAGE_ON_ERROR, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_IMAGE_ERROR));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_IMAGE_ON_SVG_PLAY_FINISH, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_IMAGE_SVG_PLAY_FINISH));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_IMAGE_ON_DOWNLOAD_PROGRESS, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_IMAGE_DOWNLOAD_PROGRESS));

    nodeType = static_cast<int32_t>(ARKUI_NODE_DATE_PICKER);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_DATE_PICKER_EVENT_ON_DATE_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_DATE_PICKER_DATE_CHANGE));

    nodeType = static_cast<int32_t>(ARKUI_NODE_TIME_PICKER);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TIME_PICKER_EVENT_ON_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TIME_PICKER_CHANGE));

    nodeType = static_cast<int32_t>(ARKUI_NODE_TEXT_PICKER);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_PICKER_EVENT_ON_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_TEXT_PICKER_CHANGE));

    nodeType = static_cast<int32_t>(ARKUI_NODE_CALENDAR_PICKER);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_CALENDAR_PICKER_EVENT_ON_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_CALENDAR_PICKER_CHANGE));

    nodeType = static_cast<int32_t>(ARKUI_NODE_CHECKBOX);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_CHECKBOX_EVENT_ON_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_CHECKBOX_CHANGE));

    nodeType = static_cast<int32_t>(ARKUI_NODE_SLIDER);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SLIDER_EVENT_ON_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_SLIDER_CHANGE));

    nodeType = static_cast<int32_t>(ARKUI_NODE_SWIPER);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SWIPER_EVENT_ON_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_SWIPER_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SWIPER_EVENT_ON_ANIMATION_START, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_SWIPER_ANIMATION_START));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SWIPER_EVENT_ON_ANIMATION_END, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_SWIPER_ANIMATION_END));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SWIPER_EVENT_ON_GESTURE_SWIPE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_SWIPER_GESTURE_SWIPE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_SWIPER_EVENT_ON_CONTENT_DID_SCROLL, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_SWIPER_DID_CONTENT_SCROLL));

    nodeType = static_cast<int32_t>(ARKUI_NODE_TEXT);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_TEXT_ON_DETECT_RESULT_UPDATE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_DETECT_RESULT_UPDATE));

    nodeType = static_cast<int32_t>(ARKUI_NODE_RADIO);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_RADIO_EVENT_ON_CHANGE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_RADIO_CHANGE));

    nodeType = static_cast<int32_t>(ARKUI_NODE_IMAGE_ANIMATOR);
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_IMAGE_ANIMATOR_EVENT_ON_START, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_IMAGE_ANIMATOR_ON_START));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_IMAGE_ANIMATOR_EVENT_ON_CANCEL, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_IMAGE_ANIMATOR_ON_CANCEL));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_IMAGE_ANIMATOR_EVENT_ON_PAUSE, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_IMAGE_ANIMATOR_ON_PAUSE));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_IMAGE_ANIMATOR_EVENT_ON_REPEAT, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_IMAGE_ANIMATOR_ON_REPEAT));
    ret = OHOS::Ace::NodeModel::ConvertOriginEventType(NODE_IMAGE_ANIMATOR_EVENT_ON_FINISH, nodeType);
    EXPECT_EQ(ret, static_cast<int32_t>(ON_IMAGE_ANIMATOR_ON_FINISH));
}

/**
 * @tc.name: NativeNodeTest007
 * @tc.desc: Test ConvertToNodeEventType function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest007, TestSize.Level1)
{
    int32_t ret;
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_INPUT_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_INPUT_ON_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_INPUT_SUBMIT);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_INPUT_ON_SUBMIT));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_INPUT_CUT);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_INPUT_ON_CUT));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_INPUT_PASTE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_INPUT_ON_PASTE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_INPUT_TEXT_SELECTION_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_INPUT_ON_TEXT_SELECTION_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_INPUT_EDIT_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_INPUT_ON_EDIT_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_INPUT_CONTENT_SIZE_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_INPUT_ON_CONTENT_SIZE_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_INPUT_INPUT_FILTER_ERROR);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_INPUT_ON_INPUT_FILTER_ERROR));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_INPUT_CONTENT_SCROLL);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_INPUT_ON_CONTENT_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_INPUT_WILL_INSERT);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_INPUT_ON_WILL_INSERT));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_INPUT_DID_INSERT);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_INPUT_ON_DID_INSERT));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_INPUT_WILL_DELETE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_INPUT_ON_WILL_DELETE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_INPUT_DID_DELETE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_INPUT_ON_DID_DELETE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_SCROLL);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_SCROLL_FRAME_BEGIN);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_SCROLL_FRAME_BEGIN));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_SCROLL_WILL_SCROLL);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_WILL_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_SCROLL_DID_SCROLL);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_DID_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_SCROLL_START);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_SCROLL_START));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_SCROLL_STOP);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_SCROLL_STOP));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_SCROLL_EDGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_SCROLL_EDGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_SCROLL_REACH_START);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_REACH_START));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_SCROLL_REACH_END);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_REACH_END));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_LIST_SCROLL);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_LIST_SCROLL_FRAME_BEGIN);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_SCROLL_FRAME_BEGIN));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_LIST_SCROLL_START);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_SCROLL_START));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_LIST_SCROLL_STOP);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_SCROLL_STOP));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_LIST_REACH_START);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_REACH_START));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_LIST_REACH_END);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_REACH_END));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_LIST_SCROLL_INDEX);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_LIST_ON_SCROLL_INDEX));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_LIST_DID_SCROLL);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_LIST_ON_DID_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_LIST_WILL_SCROLL);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_LIST_ON_WILL_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_WATER_FLOW_SCROLL_FRAME_BEGIN);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_SCROLL_FRAME_BEGIN));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_WATER_FLOW_SCROLL_START);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_SCROLL_START));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_WATER_FLOW_SCROLL_STOP);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_SCROLL_STOP));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_WATER_FLOW_WILL_SCROLL);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_ON_WILL_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_WATER_FLOW_REACH_START);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_REACH_START));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_WATER_FLOW_REACH_END);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SCROLL_EVENT_ON_REACH_END));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_WATER_FLOW_DID_SCROLL);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_WATER_FLOW_ON_DID_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_WATER_FLOW_SCROLL_INDEX);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_WATER_FLOW_ON_SCROLL_INDEX));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_APPEAR);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_EVENT_ON_APPEAR));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_DISAPPEAR);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_EVENT_ON_DISAPPEAR));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_AREA_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_EVENT_ON_AREA_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_FOCUS);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_ON_FOCUS));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_BLUR);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_ON_BLUR));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TOUCH);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TOUCH_EVENT));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_CLICK);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_ON_CLICK));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TOUCH_INTERCEPT);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_ON_TOUCH_INTERCEPT));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_VISIBLE_AREA_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_EVENT_ON_VISIBLE_AREA_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_HOVER);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_ON_HOVER));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_MOUSE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_ON_MOUSE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_ATTACH);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_EVENT_ON_ATTACH));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_DETACH);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_EVENT_ON_DETACH));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_ACCESSIBILITY_ACTIONS);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_ON_ACCESSIBILITY_ACTIONS));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXTAREA_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_AREA_ON_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_AREA_INPUT_FILTER_ERROR);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_AREA_ON_INPUT_FILTER_ERROR));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXTAREA_EDIT_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_AREA_ON_EDIT_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXTAREA_ON_SUBMIT);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_AREA_ON_SUBMIT));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXTAREA_CONTENT_SIZE_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_AREA_ON_CONTENT_SIZE_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXTAREA_PASTE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_AREA_ON_PASTE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXTAREA_TEXT_SELECTION_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_AREA_ON_TEXT_SELECTION_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_AREA_CONTENT_SCROLL);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_AREA_ON_CONTENT_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_AREA_WILL_INSERT);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_AREA_ON_WILL_INSERT));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_AREA_DID_INSERT);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_AREA_ON_DID_INSERT));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_AREA_WILL_DELETE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_AREA_ON_WILL_DELETE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_AREA_DID_DELETE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_AREA_ON_DID_DELETE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_REFRESH_STATE_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_REFRESH_STATE_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_REFRESH_REFRESHING);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_REFRESH_ON_REFRESH));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_REFRESH_ON_OFFSET_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_REFRESH_ON_OFFSET_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TOGGLE_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TOGGLE_ON_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_IMAGE_COMPLETE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_IMAGE_ON_COMPLETE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_IMAGE_ERROR);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_IMAGE_ON_ERROR));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_IMAGE_SVG_PLAY_FINISH);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_IMAGE_ON_SVG_PLAY_FINISH));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_IMAGE_DOWNLOAD_PROGRESS);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_IMAGE_ON_DOWNLOAD_PROGRESS));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_DATE_PICKER_DATE_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_DATE_PICKER_EVENT_ON_DATE_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TIME_PICKER_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TIME_PICKER_EVENT_ON_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_TEXT_PICKER_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_PICKER_EVENT_ON_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_CALENDAR_PICKER_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_CALENDAR_PICKER_EVENT_ON_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_CHECKBOX_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_CHECKBOX_EVENT_ON_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_SLIDER_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SLIDER_EVENT_ON_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_SWIPER_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SWIPER_EVENT_ON_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_SWIPER_ANIMATION_START);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SWIPER_EVENT_ON_ANIMATION_START));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_SWIPER_ANIMATION_END);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SWIPER_EVENT_ON_ANIMATION_END));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_SWIPER_GESTURE_SWIPE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SWIPER_EVENT_ON_GESTURE_SWIPE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_SWIPER_DID_CONTENT_SCROLL);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_SWIPER_EVENT_ON_CONTENT_DID_SCROLL));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_DETECT_RESULT_UPDATE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_TEXT_ON_DETECT_RESULT_UPDATE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_RADIO_CHANGE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_RADIO_EVENT_ON_CHANGE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_IMAGE_ANIMATOR_ON_START);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_IMAGE_ANIMATOR_EVENT_ON_START));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_IMAGE_ANIMATOR_ON_CANCEL);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_IMAGE_ANIMATOR_EVENT_ON_CANCEL));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_IMAGE_ANIMATOR_ON_PAUSE);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_IMAGE_ANIMATOR_EVENT_ON_PAUSE));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_IMAGE_ANIMATOR_ON_REPEAT);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_IMAGE_ANIMATOR_EVENT_ON_REPEAT));
    ret = OHOS::Ace::NodeModel::ConvertToNodeEventType(ON_IMAGE_ANIMATOR_ON_FINISH);
    EXPECT_EQ(ret, static_cast<int32_t>(NODE_IMAGE_ANIMATOR_EVENT_ON_FINISH));
}

/**
 * @tc.name: NativeNodeTest008
 * @tc.desc: Test textNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest008, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_TEXT);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_CONTENT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_WEIGHT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_LINE_HEIGHT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_DECORATION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_CASE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_LETTER_SPACING), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_MAX_LINES), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_ALIGN), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_OVERFLOW), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_FAMILY), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_COPY_OPTION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_BASELINE_OFFSET), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_TEXT_SHADOW), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_MIN_FONT_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_MAX_FONT_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_FONT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_HEIGHT_ADAPTIVE_POLICY), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INDENT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_WORD_BREAK), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_ELLIPSIS_MODE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_LINE_SPACING), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_FONT_FEATURE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_ENABLE_DATA_DETECTOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_ENABLE_DATA_DETECTOR_CONFIG), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_SELECTED_BACKGROUND_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_CONTENT_WITH_STYLED_STRING), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest009
 * @tc.desc: Test spanNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest009, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_SPAN);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SPAN_CONTENT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SPAN_TEXT_BACKGROUND_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SPAN_BASELINE_OFFSET), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_DECORATION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_WEIGHT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_LINE_HEIGHT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_CASE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_LETTER_SPACING), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_FAMILY), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_TEXT_SHADOW), ARKUI_ERROR_CODE_NO_ERROR);

    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_WIDTH, nullptr), ARKUI_ERROR_CODE_ATTRIBUTE_OR_EVENT_NOT_SUPPORTED);
    EXPECT_EQ(nodeAPI->getAttribute(rootNode, NODE_WIDTH), nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_WIDTH), ARKUI_ERROR_CODE_ATTRIBUTE_OR_EVENT_NOT_SUPPORTED);
}

/**
 * @tc.name: NativeNodeTest010
 * @tc.desc: Test imageSpanNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest010, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_IMAGE_SPAN);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_SPAN_SRC), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_SPAN_VERTICAL_ALIGNMENT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_SPAN_ALT), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest011
 * @tc.desc: Test imageNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest011, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_IMAGE);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_SRC), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_OBJECT_FIT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_INTERPOLATION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_OBJECT_REPEAT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_COLOR_FILTER), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_AUTO_RESIZE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_ALT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_DRAGGABLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_RENDER_MODE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_FIT_ORIGINAL_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_FILL_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_RESIZABLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BORDER_RADIUS), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BORDER_RADIUS_PERCENT), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest012
 * @tc.desc: Test toggleNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest012, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_TOGGLE);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TOGGLE_SELECTED_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TOGGLE_SWITCH_POINT_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TOGGLE_VALUE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TOGGLE_UNSELECTED_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest013
 * @tc.desc: Test loadingProgressNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest013, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_LOADING_PROGRESS);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_LOADING_PROGRESS_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_LOADING_PROGRESS_ENABLE_LOADING), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest014
 * @tc.desc: Test textinputNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest014, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_TEXT_INPUT);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_PLACEHOLDER), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_TEXT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_CARET_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_CARET_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_SHOW_UNDERLINE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_MAX_LENGTH), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_ENTER_KEY_TYPE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_PLACEHOLDER_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_PLACEHOLDER_FONT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_ENABLE_KEYBOARD_ON_FOCUS), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_TYPE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_SELECTED_BACKGROUND_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_SHOW_PASSWORD_ICON), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_EDITING), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_CANCEL_BUTTON), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_TEXT_SELECTION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_UNDERLINE_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_ENABLE_AUTO_FILL), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_CONTENT_TYPE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_PASSWORD_RULES), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_SELECT_ALL), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_INPUT_FILTER), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_CARET_OFFSET), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_CONTENT_RECT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_CONTENT_LINE_COUNT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_SELECTION_MENU_HIDDEN), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_BLUR_ON_SUBMIT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_CUSTOM_KEYBOARD), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_WORD_BREAK), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_SHOW_KEYBOARD_ON_FOCUS), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_INPUT_NUMBER_OF_LINES), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BACKGROUND_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_PADDING), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_MARGIN), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BORDER_WIDTH), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BORDER_RADIUS), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BORDER_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BORDER_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_WEIGHT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_FAMILY), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_MIN_FONT_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_MAX_FONT_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_LINE_HEIGHT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_MAX_LINES), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_ALIGN), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest015
 * @tc.desc: Test textareaNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest015, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_TEXT_AREA);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_PLACEHOLDER), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_TEXT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_MAX_LENGTH), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_PLACEHOLDER_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_PLACEHOLDER_FONT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_CARET_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_EDITING), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_TYPE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_SHOW_COUNTER), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_SELECTION_MENU_HIDDEN), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_BLUR_ON_SUBMIT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_INPUT_FILTER), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_SELECTED_BACKGROUND_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_ENTER_KEY_TYPE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_ENABLE_KEYBOARD_ON_FOCUS), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_CARET_OFFSET), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_CONTENT_RECT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_CONTENT_LINE_COUNT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_TEXT_SELECTION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_ENABLE_AUTO_FILL), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_CONTENT_TYPE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_SHOW_KEYBOARD_ON_FOCUS), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_AREA_NUMBER_OF_LINES), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BACKGROUND_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_MARGIN), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BORDER_WIDTH), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BORDER_RADIUS), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BORDER_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BORDER_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_WEIGHT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_FAMILY), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_MIN_FONT_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_MAX_FONT_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_LINE_HEIGHT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_MAX_LINES), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_ALIGN), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest016
 * @tc.desc: Test buttonNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest016, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_BUTTON);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BUTTON_LABEL), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BUTTON_TYPE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_BACKGROUND_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FONT_WEIGHT), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest017
 * @tc.desc: Test progressNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest017, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_PROGRESS);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_PROGRESS_VALUE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_PROGRESS_TOTAL), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_PROGRESS_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_PROGRESS_TYPE), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest018
 * @tc.desc: Test checkboxNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest018, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_CHECKBOX);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_CHECKBOX_SELECT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_CHECKBOX_SELECT_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_CHECKBOX_UNSELECT_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_CHECKBOX_MARK), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_CHECKBOX_SHAPE), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest019
 * @tc.desc: Test xcomponentNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest019, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_XCOMPONENT);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_XCOMPONENT_ID), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_XCOMPONENT_TYPE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_XCOMPONENT_SURFACE_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest020
 * @tc.desc: Test datePickerNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest020, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_DATE_PICKER);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_DATE_PICKER_LUNAR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_DATE_PICKER_START), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_DATE_PICKER_END), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_DATE_PICKER_SELECTED), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_DATE_PICKER_DISAPPEAR_TEXT_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_DATE_PICKER_TEXT_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_DATE_PICKER_SELECTED_TEXT_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest021
 * @tc.desc: Test timePickerNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest021, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_TIME_PICKER);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TIME_PICKER_SELECTED), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TIME_PICKER_USE_MILITARY_TIME), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TIME_PICKER_DISAPPEAR_TEXT_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TIME_PICKER_TEXT_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TIME_PICKER_SELECTED_TEXT_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest022
 * @tc.desc: Test textPickerNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest022, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_TEXT_PICKER);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_PICKER_OPTION_RANGE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_PICKER_OPTION_SELECTED), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_PICKER_OPTION_VALUE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_PICKER_DISAPPEAR_TEXT_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_PICKER_TEXT_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_PICKER_SELECTED_TEXT_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_PICKER_SELECTED_INDEX), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_TEXT_PICKER_CAN_LOOP), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(
        rootNode, NODE_TEXT_PICKER_DEFAULT_PICKER_ITEM_HEIGHT), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest023
 * @tc.desc: Test calendarPickerNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest023, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_CALENDAR_PICKER);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_CALENDAR_PICKER_HINT_RADIUS), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_CALENDAR_PICKER_SELECTED_DATE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_CALENDAR_PICKER_EDGE_ALIGNMENT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_CALENDAR_PICKER_TEXT_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest024
 * @tc.desc: Test sliderNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest024, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_SLIDER);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SLIDER_BLOCK_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SLIDER_TRACK_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SLIDER_SELECTED_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SLIDER_SHOW_STEPS), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SLIDER_BLOCK_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SLIDER_VALUE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SLIDER_MIN_VALUE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SLIDER_MAX_VALUE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SLIDER_STEP), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SLIDER_DIRECTION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SLIDER_REVERSE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SLIDER_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SLIDER_TRACK_THICKNESS), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest025
 * @tc.desc: Test radioNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest025, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_RADIO);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_RADIO_CHECKED), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_RADIO_STYLE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_RADIO_VALUE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_RADIO_GROUP), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest026
 * @tc.desc: Test scrollNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest026, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_SCROLL);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SCROLL_BAR_DISPLAY_MODE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SCROLL_BAR_WIDTH), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SCROLL_BAR_COLOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SCROLL_SCROLL_DIRECTION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SCROLL_EDGE_EFFECT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SCROLL_ENABLE_SCROLL_INTERACTION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SCROLL_FRICTION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SCROLL_SNAP), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SCROLL_NESTED_SCROLL), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SCROLL_OFFSET), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SCROLL_EDGE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SCROLL_ENABLE_PAGING), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SCROLL_PAGE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SCROLL_BY), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest027
 * @tc.desc: Test listNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest027, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_LIST);
    auto childNode = nodeAPI->createNode(ARKUI_NODE_LIST_ITEM);
    auto groupNode = nodeAPI->createNode(ARKUI_NODE_LIST_ITEM_GROUP);
    ASSERT_NE(rootNode, nullptr);
    ASSERT_NE(childNode, nullptr);
    ASSERT_NE(groupNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_LIST_DIRECTION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_LIST_STICKY), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_LIST_SPACE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_LIST_NODE_ADAPTER), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_LIST_CACHED_COUNT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_LIST_SCROLL_TO_INDEX), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_LIST_ALIGN_LIST_ITEM), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_LIST_CHILDREN_MAIN_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_LIST_INITIAL_INDEX), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_LIST_DIVIDER), ARKUI_ERROR_CODE_NO_ERROR);

    EXPECT_EQ(nodeAPI->resetAttribute(childNode, NODE_LIST_ITEM_SWIPE_ACTION), ARKUI_ERROR_CODE_NO_ERROR);

    EXPECT_EQ(nodeAPI->resetAttribute(groupNode, NODE_LIST_ITEM_GROUP_SET_HEADER), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(groupNode, NODE_LIST_ITEM_GROUP_SET_FOOTER), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(groupNode, NODE_LIST_ITEM_GROUP_SET_DIVIDER), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(groupNode, NODE_LIST_ITEM_GROUP_CHILDREN_MAIN_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest028
 * @tc.desc: Test swiperNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest028, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_SWIPER);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_LOOP), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_AUTO_PLAY), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_SHOW_INDICATOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_INTERVAL), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_VERTICAL), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_DURATION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_CURVE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_ITEM_SPACE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_INDEX), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_DISPLAY_COUNT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_DISABLE_SWIPE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_SHOW_DISPLAY_ARROW), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_EDGE_EFFECT_MODE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_NODE_ADAPTER), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_CACHED_COUNT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_PREV_MARGIN), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_NEXT_MARGIN), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_INDICATOR), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_NESTED_SCROLL), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_SWIPE_TO_INDEX), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_SWIPER_INDICATOR_INTERACTIVE), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest029
 * @tc.desc: Test columnNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest029, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_COLUMN);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_COLUMN_ALIGN_ITEMS), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_COLUMN_JUSTIFY_CONTENT), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest030
 * @tc.desc: Test rowNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest030, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_ROW);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_ROW_ALIGN_ITEMS), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_ROW_JUSTIFY_CONTENT), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest031
 * @tc.desc: Test flexNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest031, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_FLEX);
    auto childNode = nodeAPI->createNode(ARKUI_NODE_STACK);
    ASSERT_NE(rootNode, nullptr);
    ASSERT_NE(childNode, nullptr);
    nodeAPI->addChild(rootNode, childNode);

    ArkUI_NumberValue value0[] = {};
    ArkUI_AttributeItem item0 = {value0, 0, nullptr, nullptr};
    int32_t negativeEnum = -1;
    ArkUI_NumberValue valueEnum[] = {{.i32 = negativeEnum}};
    ArkUI_AttributeItem itemEnum = {valueEnum, 1, nullptr, nullptr};
    float negativeFloat = -1.0f;
    ArkUI_NumberValue valueFloat[] = {{.f32 = negativeFloat}};
    ArkUI_AttributeItem itemFloat = {valueFloat, sizeof(valueFloat) / sizeof(ArkUI_NumberValue)};
    ArkUI_NumberValue value[] = {{.i32 = ARKUI_ITEM_ALIGNMENT_AUTO}};
    ArkUI_AttributeItem item = {value, sizeof(value) / sizeof(ArkUI_NumberValue), nullptr, nullptr};
    EXPECT_EQ(nodeAPI->setAttribute(childNode, NODE_ALIGN_SELF, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(childNode, NODE_ALIGN_SELF, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(childNode, NODE_ALIGN_SELF, &item), ARKUI_ERROR_CODE_NO_ERROR);
    auto alignSelfVal = nodeAPI->getAttribute(childNode, NODE_ALIGN_SELF);
    EXPECT_EQ(alignSelfVal->value[0].i32, static_cast<int32_t>(ARKUI_ITEM_ALIGNMENT_AUTO));

    EXPECT_EQ(nodeAPI->setAttribute(childNode, NODE_FLEX_GROW, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(childNode, NODE_FLEX_GROW, &itemFloat), ARKUI_ERROR_CODE_PARAM_INVALID);
    float flexGrowInput = 2.0f;
    value[0].f32 = flexGrowInput;
    EXPECT_EQ(nodeAPI->setAttribute(childNode, NODE_FLEX_GROW, &item), ARKUI_ERROR_CODE_NO_ERROR);
    auto flexGrowVal = nodeAPI->getAttribute(childNode, NODE_FLEX_GROW);
    EXPECT_EQ(flexGrowVal->value[0].f32, flexGrowInput);

    EXPECT_EQ(nodeAPI->setAttribute(childNode, NODE_FLEX_SHRINK, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(childNode, NODE_FLEX_SHRINK, &itemFloat), ARKUI_ERROR_CODE_PARAM_INVALID);
    float flexShrinkInput = 0.0f;
    value[0].f32 = flexShrinkInput;
    EXPECT_EQ(nodeAPI->setAttribute(childNode, NODE_FLEX_SHRINK, &item), ARKUI_ERROR_CODE_NO_ERROR);
    auto flexShrinkVal = nodeAPI->getAttribute(childNode, NODE_FLEX_SHRINK);
    EXPECT_EQ(flexShrinkVal->value[0].f32, flexShrinkInput);

    EXPECT_EQ(nodeAPI->setAttribute(childNode, NODE_FLEX_BASIS, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(childNode, NODE_FLEX_BASIS, &itemFloat), ARKUI_ERROR_CODE_PARAM_INVALID);
    float flexBasisInput = 100.0f;
    value[0].f32 = flexBasisInput;
    EXPECT_EQ(nodeAPI->setAttribute(childNode, NODE_FLEX_BASIS, &item), ARKUI_ERROR_CODE_NO_ERROR);
    auto flexBasisVal = nodeAPI->getAttribute(childNode, NODE_FLEX_BASIS);
    EXPECT_EQ(flexBasisVal->value[0].f32, flexBasisInput);

    ArkUI_NumberValue flexOptVal[] = {{.i32 = ARKUI_FLEX_DIRECTION_ROW}, {.i32 = ARKUI_FLEX_WRAP_NO_WRAP},
        {.i32 = ARKUI_FLEX_ALIGNMENT_START}, {.i32 = ARKUI_ITEM_ALIGNMENT_START}, {.i32 = ARKUI_FLEX_ALIGNMENT_START}};
    ArkUI_AttributeItem flexOptItem = {flexOptVal, sizeof(flexOptVal) / sizeof(ArkUI_NumberValue), nullptr, nullptr};
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_FLEX_OPTION, &flexOptItem), ARKUI_ERROR_CODE_NO_ERROR);
    auto flexOptResult = nodeAPI->getAttribute(rootNode, NODE_FLEX_OPTION);
    EXPECT_EQ(flexOptResult->value[0].i32, static_cast<int32_t>(ARKUI_FLEX_DIRECTION_ROW));

    int32_t abnormalVal = 10;
    flexOptVal[4].i32 = abnormalVal;
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_FLEX_OPTION, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_FLEX_OPTION, &flexOptItem), ARKUI_ERROR_CODE_PARAM_INVALID);
    flexOptVal[3].i32 = abnormalVal;
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_FLEX_OPTION, &flexOptItem), ARKUI_ERROR_CODE_PARAM_INVALID);
    flexOptVal[2].i32 = abnormalVal;
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_FLEX_OPTION, &flexOptItem), ARKUI_ERROR_CODE_PARAM_INVALID);
    flexOptVal[1].i32 = abnormalVal;
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_FLEX_OPTION, &flexOptItem), ARKUI_ERROR_CODE_PARAM_INVALID);
    flexOptVal[0].i32 = abnormalVal;
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_FLEX_OPTION, &flexOptItem), ARKUI_ERROR_CODE_PARAM_INVALID);

    EXPECT_EQ(nodeAPI->resetAttribute(childNode, NODE_ALIGN_SELF), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(childNode, NODE_FLEX_GROW), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(childNode, NODE_FLEX_SHRINK), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(childNode, NODE_FLEX_BASIS), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_FLEX_OPTION), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest032
 * @tc.desc: Test refreshNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest032, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_REFRESH);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_REFRESH_REFRESHING), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_REFRESH_CONTENT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_REFRESH_PULL_DOWN_RATIO), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_REFRESH_OFFSET), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_REFRESH_PULL_TO_REFRESH), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest033
 * @tc.desc: Test waterflowNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest033, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_WATER_FLOW);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_WATER_FLOW_LAYOUT_DIRECTION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_WATER_FLOW_COLUMN_TEMPLATE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_WATER_FLOW_ROW_TEMPLATE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_WATER_FLOW_COLUMN_GAP), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_WATER_FLOW_ROW_GAP), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_WATER_FLOW_SECTION_OPTION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_WATER_FLOW_NODE_ADAPTER), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_WATER_FLOW_CACHED_COUNT), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_WATER_FLOW_FOOTER), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_WATER_FLOW_SCROLL_TO_INDEX), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_WATER_FLOW_ITEM_CONSTRAINT_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest034
 * @tc.desc: Test gridNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest034, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_GRID);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_GRID_COLUMN_TEMPLATE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_GRID_ROW_TEMPLATE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_GRID_COLUMN_GAP), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_GRID_ROW_GAP), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_GRID_NODE_ADAPTER), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_GRID_CACHED_COUNT), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest035
 * @tc.desc: Test relativeContainerNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest035, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_RELATIVE_CONTAINER);
    ASSERT_NE(rootNode, nullptr);

    auto alignRules = OH_ArkUI_AlignmentRuleOption_Create();
    float baisVal = 10.0f;
    OH_ArkUI_AlignmentRuleOption_SetStart(alignRules, "__container__", ARKUI_HORIZONTAL_ALIGNMENT_START);
    OH_ArkUI_AlignmentRuleOption_SetTop(alignRules, "__container__", ARKUI_VERTICAL_ALIGNMENT_TOP);
    OH_ArkUI_AlignmentRuleOption_SetEnd(alignRules, "__container__", ARKUI_HORIZONTAL_ALIGNMENT_END);
    OH_ArkUI_AlignmentRuleOption_SetBottom(alignRules, "__container__", ARKUI_VERTICAL_ALIGNMENT_BOTTOM);
    OH_ArkUI_AlignmentRuleOption_SetCenterHorizontal(alignRules, "__container__", ARKUI_HORIZONTAL_ALIGNMENT_CENTER);
    OH_ArkUI_AlignmentRuleOption_SetCenterVertical(alignRules, "__container__", ARKUI_VERTICAL_ALIGNMENT_CENTER);
    OH_ArkUI_AlignmentRuleOption_SetBiasHorizontal(alignRules, baisVal);
    OH_ArkUI_AlignmentRuleOption_SetBiasVertical(alignRules, baisVal);
    ArkUI_AttributeItem alignRulesItem = {.size = 0, .object = alignRules};
    nodeAPI->setAttribute(rootNode, NODE_ALIGN_RULES, &alignRulesItem);
    auto alignRulesGetVal = nodeAPI->getAttribute(rootNode, NODE_ALIGN_RULES);
    ASSERT_NE(alignRulesGetVal, nullptr);

    ArkUI_NumberValue chainModeVal[] = {{.i32 = ARKUI_AXIS_HORIZONTAL},
        {.i32 = ARKUI_RELATIVE_LAYOUT_CHAIN_STYLE_PACKED}};
    ArkUI_AttributeItem chainModeItem = {chainModeVal, sizeof(chainModeVal) / sizeof(ArkUI_NumberValue)};
    auto chainModRet = nodeAPI->setAttribute(rootNode, NODE_RELATIVE_LAYOUT_CHAIN_MODE, &chainModeItem);
    EXPECT_EQ(chainModRet, ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->setAttribute(
        rootNode, NODE_RELATIVE_LAYOUT_CHAIN_MODE, nullptr), ARKUI_ERROR_CODE_PARAM_INVALID);
    auto chainModResult = nodeAPI->getAttribute(rootNode, NODE_RELATIVE_LAYOUT_CHAIN_MODE);
    EXPECT_EQ(chainModResult->value[0].i32, static_cast<int32_t>(ARKUI_AXIS_HORIZONTAL));

    int32_t guideLineOptSize = 1;
    float positionStart = 50.0f;
    float positionEnd = 60.0f;
    auto guidelineOption = OH_ArkUI_GuidelineOption_Create(guideLineOptSize);
    OH_ArkUI_GuidelineOption_SetId(guidelineOption, "__container__", 0);
    OH_ArkUI_GuidelineOption_SetDirection(guidelineOption, ARKUI_AXIS_VERTICAL, 0);
    OH_ArkUI_GuidelineOption_SetPositionStart(guidelineOption, positionStart, 0);
    OH_ArkUI_GuidelineOption_SetPositionEnd(guidelineOption, positionEnd, 0);
    ArkUI_AttributeItem guidelineOptItem = {.object = guidelineOption};
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_RELATIVE_CONTAINER_GUIDE_LINE, &guidelineOptItem),
        ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_RELATIVE_CONTAINER_GUIDE_LINE, nullptr),
        ARKUI_ERROR_CODE_PARAM_INVALID);
    auto guidelineOptResult = nodeAPI->getAttribute(rootNode, NODE_RELATIVE_CONTAINER_GUIDE_LINE);
    ASSERT_NE(guidelineOptResult, nullptr);
    
    auto barrierOption = OH_ArkUI_BarrierOption_Create(guideLineOptSize);
    OH_ArkUI_BarrierOption_SetId(barrierOption, "__container__", 0);
    OH_ArkUI_BarrierOption_SetDirection(barrierOption, ARKUI_BARRIER_DIRECTION_TOP, 0);
    OH_ArkUI_BarrierOption_SetReferencedId(barrierOption, "__container__", 0);
    ArkUI_AttributeItem barrierOptItem = {.object = barrierOption};
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_RELATIVE_CONTAINER_BARRIER, &barrierOptItem),
        ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_RELATIVE_CONTAINER_BARRIER, nullptr),
        ARKUI_ERROR_CODE_PARAM_INVALID);
    auto barrierOptResult = nodeAPI->getAttribute(rootNode, NODE_RELATIVE_CONTAINER_BARRIER);
    ASSERT_NE(barrierOptResult, nullptr);

    OH_ArkUI_AlignmentRuleOption_Dispose(alignRules);
    OH_ArkUI_GuidelineOption_Dispose(guidelineOption);
    OH_ArkUI_BarrierOption_Dispose(barrierOption);

    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_ALIGN_RULES), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_RELATIVE_LAYOUT_CHAIN_MODE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_RELATIVE_CONTAINER_GUIDE_LINE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_RELATIVE_CONTAINER_BARRIER), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest036
 * @tc.desc: Test imageAnimatorNode function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest036, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_IMAGE_ANIMATOR);
    ASSERT_NE(rootNode, nullptr);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_ANIMATOR_IMAGES), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_ANIMATOR_STATE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_ANIMATOR_DURATION), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_ANIMATOR_REVERSE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_ANIMATOR_FIXED_SIZE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_ANIMATOR_FILL_MODE), ARKUI_ERROR_CODE_NO_ERROR);
    EXPECT_EQ(nodeAPI->resetAttribute(rootNode, NODE_IMAGE_ANIMATOR_ITERATION), ARKUI_ERROR_CODE_NO_ERROR);
}

/**
 * @tc.name: NativeNodeTest037
 * @tc.desc: Test abnormalInputForCommon function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest037, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_STACK);
    float negativeFloat = -1.0f;
    int32_t negativeEnum = -1;
    ArkUI_NumberValue value[] = {{.f32 = negativeFloat}};
    ArkUI_AttributeItem item = {value, sizeof(value) / sizeof(ArkUI_NumberValue)};
    ArkUI_NumberValue value4[] = {{.f32 = negativeFloat}, {.f32 = negativeFloat}, {.f32 = negativeFloat},
        {.f32 = negativeFloat}};
    ArkUI_AttributeItem item4 = {value4, sizeof(value4) / sizeof(ArkUI_NumberValue)};
    ArkUI_NumberValue value0[] = {};
    ArkUI_AttributeItem item0 = {value0, 0, nullptr, nullptr};
    ArkUI_NumberValue valueEnum[] = {{.i32 = negativeEnum}};
    ArkUI_AttributeItem itemEnum = {valueEnum, 1, "test", nullptr};
    ArkUI_NumberValue valueEnum4[] = {{.i32 = negativeEnum}, {.i32 = negativeEnum}, {.i32 = negativeEnum},
        {.i32 = negativeEnum}};
    ArkUI_AttributeItem itemEnum4 = {valueEnum4, sizeof(valueEnum4) / sizeof(ArkUI_NumberValue), nullptr, nullptr};
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_WIDTH, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_WIDTH, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_HEIGHT, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_HEIGHT, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_WIDTH_PERCENT, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_WIDTH_PERCENT, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_HEIGHT_PERCENT, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_HEIGHT_PERCENT, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BACKGROUND_COLOR, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BACKGROUND_IMAGE, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BACKGROUND_IMAGE, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_PADDING, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_ID, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_ENABLED, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_ENABLED, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_MARGIN, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TRANSLATE, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_SCALE, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_SCALE, nullptr), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_ROTATE, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BRIGHTNESS, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BRIGHTNESS, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_SATURATION, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_SATURATION, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BLUR, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_LINEAR_GRADIENT, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_LINEAR_GRADIENT, &item4), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_ALIGNMENT, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_ALIGNMENT, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_OPACITY, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_OPACITY, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BORDER_WIDTH, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BORDER_WIDTH, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BORDER_WIDTH, &item4), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BORDER_WIDTH_PERCENT, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BORDER_WIDTH_PERCENT, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BORDER_WIDTH_PERCENT, &item4), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BORDER_RADIUS, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BORDER_RADIUS, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BORDER_RADIUS, &item4), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BORDER_RADIUS_PERCENT, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BORDER_RADIUS_PERCENT, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BORDER_RADIUS_PERCENT, &item4), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BORDER_COLOR, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BORDER_STYLE, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BORDER_STYLE, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BORDER_STYLE, &itemEnum4), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_Z_INDEX, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_VISIBILITY, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_VISIBILITY, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_VISIBILITY, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_CLIP, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_CLIP, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_CLIP_SHAPE, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_CLIP_SHAPE, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TRANSFORM, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_HIT_TEST_BEHAVIOR, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_HIT_TEST_BEHAVIOR, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_POSITION, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_SHADOW, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_SHADOW, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_CUSTOM_SHADOW, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_CUSTOM_SHADOW, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_FOCUSABLE, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_FOCUSABLE, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_ACCESSIBILITY_GROUP, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_ACCESSIBILITY_GROUP, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_ACCESSIBILITY_TEXT, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_ACCESSIBILITY_MODE, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_ACCESSIBILITY_MODE, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_ACCESSIBILITY_DESCRIPTION, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_ACCESSIBILITY_ACTIONS, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_ACCESSIBILITY_ROLE, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_DEFAULT_FOCUS, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_DEFAULT_FOCUS, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_RESPONSE_REGION, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_OVERLAY, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_OVERLAY, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_BACKGROUND_IMAGE_POSITION, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_SWEEP_GRADIENT, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_SWEEP_GRADIENT, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);

    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_ASPECT_RATIO, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_ASPECT_RATIO, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    nodeAPI->disposeNode(rootNode);
}

/**
 * @tc.name: NativeNodeTest038
 * @tc.desc: Test abnormalInputForText function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest038, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_TEXT);
    float negativeFloat = -1.0f;
    int32_t negativeInt = -1;
    ArkUI_NumberValue value[] = {{.f32 = negativeFloat}};
    ArkUI_AttributeItem item = {value, sizeof(value) / sizeof(ArkUI_NumberValue), nullptr, nullptr};
    ArkUI_NumberValue value0[] = {};
    ArkUI_AttributeItem item0 = {value0, 0, nullptr, nullptr};
    ArkUI_NumberValue value4[] = {{.f32 = negativeFloat}, {.f32 = negativeFloat}, {.f32 = negativeFloat},
        {.f32 = negativeFloat}};
    ArkUI_AttributeItem item4 = {value4, sizeof(value4) / sizeof(ArkUI_NumberValue), nullptr, nullptr};
    ArkUI_NumberValue valueEnum[] = {{.i32 = negativeInt}};
    ArkUI_AttributeItem itemEnum = {valueEnum, sizeof(valueEnum) / sizeof(ArkUI_NumberValue), nullptr, nullptr};
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_CONTENT, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_FONT_COLOR, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_FONT_SIZE, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_FONT_STYLE, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_FONT_WEIGHT, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_LINE_HEIGHT, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_DECORATION, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_CASE, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_CASE, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_LETTER_SPACING, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_MAX_LINES, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_ALIGN, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_ALIGN, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_OVERFLOW, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_OVERFLOW, &itemEnum), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_FONT_FAMILY, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_COPY_OPTION, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_BASELINE_OFFSET, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_TEXT_SHADOW, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_MIN_FONT_SIZE, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_MAX_FONT_SIZE, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_FONT, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    value4[1].i32 = negativeInt;
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_FONT, &item4), ARKUI_ERROR_CODE_PARAM_INVALID);
    value4[1].i32 = 0;
    value4[2].i32 = negativeInt;
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_FONT, &item4), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_HEIGHT_ADAPTIVE_POLICY, &item0),
        ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_INDENT, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_WORD_BREAK, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_ELLIPSIS_MODE, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_LINE_SPACING, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_FONT_FEATURE, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_ENABLE_DATA_DETECTOR, &item0), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_SELECTED_BACKGROUND_COLOR, &item0),
        ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_CONTENT_WITH_STYLED_STRING, &item0),
        ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_TEXT_CONTENT_WITH_STYLED_STRING, nullptr),
        ARKUI_ERROR_CODE_PARAM_INVALID);
    nodeAPI->disposeNode(rootNode);
}

/**
 * @tc.name: NativeNodeTest039
 * @tc.desc: Test abnormalInputForTextField function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest039, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto textinput = nodeAPI->createNode(ARKUI_NODE_TEXT_INPUT);
    auto textarea = nodeAPI->createNode(ARKUI_NODE_TEXT_AREA);
    float negativeFloat = -1.0f;
    ArkUI_NumberValue value[] = {{.f32 = negativeFloat}};
    ArkUI_AttributeItem item = {value, sizeof(value) / sizeof(ArkUI_NumberValue)};
    EXPECT_EQ(nodeAPI->setAttribute(textinput, NODE_TEXT_INPUT_CARET_STYLE, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(
        textinput, NODE_TEXT_INPUT_PLACEHOLDER_FONT, nullptr), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(
        textinput, NODE_TEXT_INPUT_PLACEHOLDER_FONT, &item), ARKUI_ERROR_CODE_PARAM_INVALID);

    EXPECT_EQ(nodeAPI->setAttribute(
        textarea, NODE_TEXT_INPUT_PLACEHOLDER_FONT, nullptr), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(
        textarea, NODE_TEXT_INPUT_PLACEHOLDER_FONT, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    nodeAPI->disposeNode(textinput);
    nodeAPI->disposeNode(textarea);
}

/**
 * @tc.name: NativeNodeTest040
 * @tc.desc: Test abnormalInputForWaterflow function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest040, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_WATER_FLOW);
    float negativeFloat = -1.0f;
    ArkUI_NumberValue value[] = {{.f32 = negativeFloat}};
    ArkUI_AttributeItem item = {value, sizeof(value) / sizeof(ArkUI_NumberValue)};
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_WATER_FLOW_COLUMN_GAP, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_WATER_FLOW_ROW_GAP, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    nodeAPI->disposeNode(rootNode);
}

/**
 * @tc.name: NativeNodeTest041
 * @tc.desc: Test abnormalInputForGrid function.
 * @tc.type: FUNC
 */
HWTEST_F(NativeNodeTest, NativeNodeTest041, TestSize.Level1)
{
    auto nodeAPI = reinterpret_cast<ArkUI_NativeNodeAPI_1*>(
        OH_ArkUI_QueryModuleInterfaceByName(ARKUI_NATIVE_NODE, "ArkUI_NativeNodeAPI_1"));
    auto rootNode = nodeAPI->createNode(ARKUI_NODE_GRID);
    float negativeFloat = -1.0f;
    ArkUI_NumberValue value[] = {{.f32 = negativeFloat}};
    ArkUI_AttributeItem item = {value, sizeof(value) / sizeof(ArkUI_NumberValue)};
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_GRID_COLUMN_GAP, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    EXPECT_EQ(nodeAPI->setAttribute(rootNode, NODE_GRID_ROW_GAP, &item), ARKUI_ERROR_CODE_PARAM_INVALID);
    nodeAPI->disposeNode(rootNode);
}
